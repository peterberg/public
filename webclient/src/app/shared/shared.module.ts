// Import Angular stuff

import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

// Import flex layout and CDK

import { FlexLayoutModule } from '@angular/flex-layout';

import { CdkTableModule } from '@angular/cdk/table';

// Import Angular Material stuff

import {
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
} from '@angular/material';

// Import pipes, services, components and directives

import { CookieDialogComponent } from './components/dialogs/cookie-dialog.component';
import { LoginDialogComponent } from './components/dialogs/login-dialog.component';

import { PrivacyPolicyComponent } from './components/privacy-policy.component';
import { TermsOfUseComponent } from './components/terms-of-use.component';
import { NotFoundComponent } from './components/not-found.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    FlexLayoutModule
  ],
  declarations: [
    CookieDialogComponent,
    LoginDialogComponent,
    PrivacyPolicyComponent,
    TermsOfUseComponent,
    NotFoundComponent
  ],
  entryComponents: [
    CookieDialogComponent,
    LoginDialogComponent
  ],
  exports: [
    CommonModule,
    RouterModule,
    HttpModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    FlexLayoutModule,
    CookieDialogComponent,
    LoginDialogComponent,
    PrivacyPolicyComponent,
    TermsOfUseComponent,
    NotFoundComponent
  ]
})
export class SharedModule { }
