
// Import Angular stuff

import { Component, Output, EventEmitter } from '@angular/core';

import { UserService } from '@core/services/user.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {

  @Output()
  close: EventEmitter<void> = new EventEmitter<void>();

  public currentUser: any;

    constructor(
      private userService: UserService) {
    }

    public ngOnInit() {
      this.currentUser = this.userService.getCurrentUser();

      this.userService.userChange.subscribe(user => {
        this.currentUser = user;
      });
    }

  public onClick() {
    this.close.emit();
  }
}
