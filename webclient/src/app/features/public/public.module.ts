// Import Angular stuff

import { NgModule } from '@angular/core';

// Import shared functionality

import { SharedModule } from '@shared/shared.module';

import { PublicListComponent } from './components/list.component';
import { PublicEditComponent } from './components/edit.component';

import { PublicRouting } from './public.routing';

@NgModule({
  imports: [
    SharedModule,
    PublicRouting
  ],
  declarations: [
    PublicListComponent,
    PublicEditComponent
  ]
})
export class PublicModule { }
