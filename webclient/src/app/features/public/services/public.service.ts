// Import Angular stuff

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable()
export class PublicService {
  private readonly PATH = '/api/books';

  constructor(private httpClient: HttpClient) { }

  public search(): Observable<any[]> {
    return this.httpClient.get<any[]>(this.PATH);
  }

  public get(key: String): Observable<any> {
    return this.httpClient.get<any>(this.PATH + '/' + key);
  }
}
