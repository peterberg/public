import { Component, OnInit, Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { DataSource } from '@angular/cdk/collections';

import { PublicService } from '../services/public.service';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [PublicService]
})
export class PublicListComponent implements OnInit {

  public items: any[];

  public displayedColumns = ['id', 'author', 'title', 'year', 'pages'];

  public dataSource: PublicDataSource;

  constructor(private service: PublicService) {
  }

  ngOnInit() {
    this.dataSource = new PublicDataSource(this.service);
  }

  /* Event handlers */

  private onSuccess = (response: any) => {
    console.log('Data retrieved succesfully');

    this.items = response;
  }

  private onFailure = (response: any) => {
    console.log('Data retrival failed. Response was: ' + response.status + ' ' + response.statusText);
  }
}

@Injectable()
export class PublicDataSource extends DataSource<any> {
  constructor(private service: PublicService) {
    super();
  }

  connect(): Observable<any[]> {
    return this.service.search();
  }

  disconnect() { }
}
