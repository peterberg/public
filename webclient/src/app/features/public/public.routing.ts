// Import Angular stuff

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import shared functionality, including Material Design and Flex Layout

import { SharedModule } from 'shared/shared.module';

import { PublicListComponent } from './components/list.component';
import { PublicEditComponent } from './components/edit.component';

const routes: Routes = [
  {
    path: 'list',
    component: PublicListComponent
  },
  {
    path: 'edit/:id',
    component: PublicEditComponent
  },
  {
    path: 'edit',
    component: PublicEditComponent
  },
  {
    path: '**',
    component: PublicListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PublicRouting {
};

