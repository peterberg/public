// Import Angular stuff

import { NgModule } from '@angular/core';

// Import shared functionality

import { SharedModule } from '@shared/shared.module';

import { ProtectedListComponent } from './components/list.component';
import { ProtectedEditComponent } from './components/edit.component';

import { ProtectedRouting } from './protected.routing';

@NgModule({
  imports: [
    SharedModule,
    ProtectedRouting
  ],
  declarations: [
    ProtectedListComponent,
    ProtectedEditComponent
  ]
})
export class ProtectedModule { }
