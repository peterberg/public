// Import Angular stuff

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import shared functionality, including Material Design and Flex Layout

import { SharedModule } from 'shared/shared.module';

import { ProtectedListComponent } from './components/list.component';
import { ProtectedEditComponent } from './components/edit.component';

const routes: Routes = [
  {
    path: 'list',
    component: ProtectedListComponent
  },
  {
    path: 'edit/:id',
    component: ProtectedEditComponent
  },
  {
    path: 'edit',
    component: ProtectedEditComponent
  },
  {
    path: '**',
    component: ProtectedListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProtectedRouting {
};

