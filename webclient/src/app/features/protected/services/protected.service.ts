// Import Angular stuff

import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

const DATA = [
  {
    id: '1',
    name: 'Peter'
  },
  {
    id: '2',
    name: 'Lotte'
  },
  {
    id: '3',
    name: 'Lea'
  },
  {
    id: '4',
    name: 'Anna'
  },
  {
    id: '5',
    name: 'Jacob'
  }
]

@Injectable()
export class ProtectedService {

  public search(): Observable<any[]> {
    return of(DATA);
  }

  public get(key: string): Observable<any> {
    return of(DATA.find(item => {
      return item.id === key;
    }));
  }
}
