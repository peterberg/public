import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { ProtectedService } from '../services/protected.service';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [ProtectedService]
})
export class ProtectedEditComponent {

  public form: FormGroup;

  constructor(private service: ProtectedService, private builder: FormBuilder, private route: ActivatedRoute) {
    this.form = builder.group({
      name: []
    });

    const id = this.route.snapshot.paramMap.get('id');

    this.service.get(id)
      .subscribe(data => {
        this.form.setValue({ name: data.name });
      });
  }
}
