import { Component, OnInit, Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { DataSource } from '@angular/cdk/collections';

import { ProtectedService } from '../services/protected.service';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [ProtectedService]
})
export class ProtectedListComponent implements OnInit {

  public items: any[];

  public displayedColumns = ['id', 'name'];

  public dataSource: ProtectedDataSource;

  constructor(private service: ProtectedService) {
  }

  ngOnInit() {
    this.dataSource = new ProtectedDataSource(this.service);
  }

  /* Event handlers */

  private onSuccess = (response: any) => {
    console.log('Data retrieved succesfully');

    this.items = response;
  }

  private onFailure = (response: any) => {
    console.log('Data retrival failed. Response was: ' + response.status + ' ' + response.statusText);
  }
}

@Injectable()
export class ProtectedDataSource extends DataSource<any> {
  constructor(private service: ProtectedService) {
    super();
  }
  connect(): Observable<any[]> {
    return this.service.search();
  }

  disconnect() { }
}
