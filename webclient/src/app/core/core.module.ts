// Import Angular stuff

import { NgModule, Optional, SkipSelf } from '@angular/core';

import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Import pipes, services, interceptors, components and directives

import { AlertService } from './services/alert.service';
import { AuthenticationService } from './services/authentication.service';
import { LocalStorageService } from './services/local-storage.service';
import { TokenService } from './services/token.service';
import { UserService } from './services/user.service';
import { WindowService } from './services/window.service';

import { AuthorizationInterceptor } from './interceptors/authorization.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    AlertService,
    AuthenticationService,
    LocalStorageService,
    TokenService,
    UserService,
    WindowService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ],
})
export class CoreModule {

  /**
   * Prevent feature modules from reimporting CoreModule. CoreModule should only
   * be imported once in the RootModule.
   *
   * @see https://angular.io/docs/ts/latest/guide/ngmodule.html#prevent-reimport
   * @param parentModule
   */

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the RootModule only!');
    }
  }
}
